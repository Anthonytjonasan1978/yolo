import React from 'react';
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './containers/home.js'
// import About from '/containers/.about.js'
import Navbar from './components/Navbar'
// import Limitless from './containers/limitless.js'
// import Overcome from './containers/overcome.js'
// import Dirty from './containers/dirty.js'
import News from './containers/news.js'
import Ourstory from './containers/ourstory.js'
// import Knowledge from './containers/knowledge.js'
import Packages from './containers/packages'
import Purchase from './containers/purchase.js'
import Admin from './components/admin.js'
import axios from 'axios'
// import Users from './components/users'
import Product from './components/product'

import Login from './containers/login'
import Register from './containers/register.js'
import SecretPage from './containers/secretpage.js'
import CheckOutForm from './containers/CheckOutForm.js'
// 0: {_id: "5d9211c9fb88c9288fba5232", category: "dirty", __v: 0}
// 1: {_id: "5d9211defb88c9288fba5233", category: "limitless", __v: 0}
// 2: {_id: "5d9211e8fb88c9288fba5234", category: "overcome", __v: 0}
// 3: {_id: "5d9211f9fb88c9288fba5235", category: "knowledge", __v: 0}
 
class App extends React.Component {
  state={categories:[],
          products:[],
          users:false
        }
  componentDidMount(){
   this.getCategories()
   this.verifyToken()
  }


  verifyToken= async () => {
    console.log("response")
     try{
       const token = JSON.parse(localStorage.getItem('token'))
       const response = await axios.post('http://localhost:3001/users/verify_token',{token})
       if(response.data.ok){
           this.setState({user:true})
       }else{
           this.setState({user:false})
       }
       
     }catch(error){

     }
  }




  getCategories = async () => {
     try{
       const response = await axios.get('http://localhost:3001/categories')
       //console.log(response.data)
       this.setState({categories:response.data.categories})
     }catch(error){

     }
  }

  render(){
    return (
    	
    	
      <Router>
          <Navbar categories={this.state.categories}/>
          <Route exact path="/" render={
            (props) => <Home {...props} categories={this.state.categories}/>
          } />
          <Route exact path="/news" component={News} />
          <Route exact path="/ourstory" component={Ourstory} />
          <Route exact path="/product/:category" component={Product} />
          <Route exact path="/packages" component={Packages} />
          <Route exact path="/purchase" component={Purchase} />
          <Route exact path="/admin" component={Admin} />
          <Route exact path="/login" render={
            (props) => <Login {...props} verifyToken={this.verifyToken}/>
          } />
          <Route exact path="/register" component={Register} />
          <Route exact path="/secret-page" render={props =>< SecretPage {...props}  /> }/>
          <Route exact path="/CheckOutForm" component={CheckOutForm} />

      </Router>
      
    )

  }
} 
export default App
// import axios from 'axios

// class App extends React.Component{
//    state = {
//     category:''
//   } 
// handleChange = e =>{
//     var data = e.target.value
//     this.setState({category:data})

// }

// submitChange = e =>{
//   e.preventDefault()
//   axios.post('http://localhost:8080/category/add',{category: this.state.category})
//     .then(res=>console.log(res))
//       .catch(err=> console.log(err))

//     // on submit we join characters from the array into a string
//     console.log(this.state.category)
// }

//   render(){
//     return(
//       <form onSubmit={this.submitChange}>  
//       <input onChange={this.handleChange}>
        
//       </input>

// <button></button>
// <input onChange={this.handleChange}>
        
//       </input>

//       </form>

//   )
//   }
// }




// export default App;