import React from "react";
import { NavLink } from "react-router-dom";
import '../styles/navbar.css'


const Navbar = (props) => {
  return (
    <div style={styles.navStyles}>
      <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/"}
      >
        HOME
      </NavLink>

     <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/ourstory"}
      >
        HISTORY
      </NavLink>

      <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/news"}
      >
        NEWS
      </NavLink>

      <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/packages"}
      >
        PACKAGES
      </NavLink>

      <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/register"}
      >
        SIGN UP
      </NavLink>

      <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/login"}
      >
        LOGIN
      </NavLink>
      










    <div class="dropdown">
    <button class="dropbtn">CATEGORIES
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">

     {
      props.categories.map((ele,index)=>{
        return <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/product/"+ele.category}
      ><a href="#">{ele.category}</a></NavLink>
      })
     }
{    /* <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/limitless"}
      ><a href="#">LIMITLESS</a></NavLink>
       <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/knowledge"}
      ><a href="#">KNOWLEDGE</a></NavLink>
      <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/overcome"}
      ><a href="#">OVERCOME</a></NavLink>
      <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/dirty"}
      ><a href="#">DIRTY</a></NavLink>*/}
  </div>
      

   


      
    </div>  

           <NavLink
        exact
        activeStyle={styles.active}
        className='navLink'
        to={"/purchase"}
      >
        PURCHASE NOW
      </NavLink>
  </div>

    

  );
};

export default Navbar;

const styles = {
  navStyles: {
    display: "flex",
    height: "8vh",
    background:"black",
    color:"white",
    alignItems: "center",
    justifyContent: "space-around"
  },
  active: {

  
  color: "#00CB85",
  
    
  }
  // default: {
  //   textDecoration: "none",
  //   color: "white",
   
  // }
};

