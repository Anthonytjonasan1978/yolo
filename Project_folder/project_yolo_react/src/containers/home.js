import React from 'react'
import jump2 from '../pictures/jump2.jpeg'
import japan2 from '../pictures/japan2.jpg'
import vertigo2 from '../pictures/vertigo2.jpg'
import Mudrun from '../pictures/Mudrun.png'
import { NavLink } from 'react-router-dom'
import '../styles/home.css'
class Home extends React.Component {
 
  render(){
  return(
    <div className="container">
     {
      this.props.categories.map( (ele,i) => {
        return     <div className="homecontainer">
                   <NavLink activeStyle={styles.active}
                            className='navLink heading' to={`/product/${ele.category}`}>{ele.category}</NavLink>
                    </div>

      })
     }
  
    </div>
    
      )
  }
}

export default Home;

const styles = {
  navStyles: {
  color:"white",
  justifyContent: "space-around"
  },
  active: {

  
  color: "#00CB85",
  
    
  },
  defaultStyles: {
    textDecoration: "none"
    
   
  }
}
