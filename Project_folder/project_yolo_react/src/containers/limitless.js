import React from 'react';
import '../styles/limitless.css'
import jump2 from '../pictures/jump2.jpeg'
import Shark from '../pictures/Shark.jpg'
import { NavLink } from 'react-router-dom'
import {HashLink as Link} from "react-router-hash-link";
class limitless extends React.Component {

state={
  name:''

}
 handleClick=e =>{

this.setState({name:e.target.name})
 }
  render(){

    
   
  
    return(
        <div className= 'limitcontainer'>
            <div>
                <h1 className='heading'><NavLink
               // active Style={styles.active}
               className='navLink'
               name='jump'
               onClick={e=>this.handleClick(e)}
               activeStyle={styles.active}
               to='/limitless'>JUMP</NavLink></h1>
            </div>

            <div>
                <h1 className='heading'><NavLink
               // activeStyle={styles.active}
               className='navLink'
               name='shark cage'
               onClick={e=>this.handleClick(e)}
               activeStyle={styles.active}
               to='/limitless'>SHARK CAGE</NavLink></h1>
            </div>


   {this.state.name === 'jump' 
  ? (
  <>
  <div>
  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam</p>
   <Link to='/packages/#limitless'>Press me if you dare</Link>
  </div>
  <div><iframe width="560" height="315" src="https://www.youtube.com/embed/yHfak57o-TM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  </>)
  : this.state.name === 'shark cage' 
  ? (
  <>
  <div><iframe width="560" height="315" src="https://www.youtube.com/embed/DYpGfqEK1A4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <div>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
  <NavLink
  to='/packages/#limitless'>Press me if you dare</NavLink>
  </div>
  </>)
  : (
  <>
  <div><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam</p></div>
  <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>
  </>)
}

   </div>
     

    	
)
}
}





export default limitless;

const styles = {
  navStyles: {
  color:"white",
  justifyContent: "space-around"
  },
  active: {

  
  color: "#00CB85",
  
    
  },
  default: {
    textDecoration: "none"
    
   
  }
};