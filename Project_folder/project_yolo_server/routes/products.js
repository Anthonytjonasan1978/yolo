const router = require ('express').Router()
const controller = require('../controllers/products')

// http://localhost:3000/todos/
router.post('/add',controller.add)
router.post('/remove',controller.remove)
router.post('/update',controller.update)
router.get('/:category',controller.getByCategory)
 

module.exports = router
