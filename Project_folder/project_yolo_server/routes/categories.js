const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/categories');

router.get('/', controller.findAll);

router.post('/new', controller.insert);

router.post('/delete', controller.remove);

//router.post('/update', controller.update);

module.exports = router;