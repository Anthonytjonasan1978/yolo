const router = require("express").Router();
const controller = require("../controllers/controllers.payment");

router.post("/charge", controller.charge);

module.exports = router;
