const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductsSchema = new Schema({ 
    product: {
    type :String,
    required: true,
    unique: true
}, 

   img: {
   	type: String,
   	required: true
   },

    date: {
   	type: String,
   	required: true,
    default: new Date()
   },

   
   category:{
    type:String,
    required: true
},
    price:{
      type:Number,
      required:true
    },
    description:{
      type:String,
      required:true
    },
})




module.exports =  mongoose.model('products', ProductsSchema);

