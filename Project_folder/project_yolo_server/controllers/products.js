const products = require('../models/products')

const add = async(req,res) => {
    const {product,img,description,category,price} = req.body 
    try{
        const response = await products.create({product,category,description,price,img})
        res.send(response)
//res.send ('hello')
    }
    catch(error){
        res.send(error)
    }
}
const remove = async (req,res) => {
    const {product} = req.body
    try{
        const response = await products.remove({product})
        res.send(response)
// res.send ('hello')
    }
catch(error){
    res.send(error)

}
}

const update = async (req,res) => {
    const {_id,product,description,date,category,price,img} = req.body
    try{
        const response = await products.update({_id},{product,description,date,category,price,img})
        res.send(response)
    }
catch(error){
    res.send(error)

}
}
const getByCategory = async (req,res) => {
    console.log(req.params.category)
    const {category}=req.params
    try{
        const response = await products.find({category})
        res.send(response)
    }
catch(error){
    res.send(error)

}
}




module.exports = {
add,
remove,
update,
getByCategory,
}
